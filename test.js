const tape = require('tape');
const xml2js = require('xml2js');

process.env.CI_PROJECT_DIR = '/builds/my_group/my_project';
process.env.CI_WEB_ROOT = '/opt/drupal/web';
const stylelintFormatter = require('./index');

const mockPassingTestProject = [
  {
    source: 'file.css',
    errored: false,
    warnings: [],
    deprecations: [],
    invalidOptionWarnings: [],
    ignored: false,
  },
  {
    source: 'my_module/css/file.css',
    errored: false,
    warnings: [],
    deprecations: [],
    invalidOptionWarnings: [],
    ignored: false,
  },
  {
    source: 'web/modules/custom/my_module/css/fileA.css',
    errored: false,
    warnings: [],
    deprecations: [],
    invalidOptionWarnings: [],
    ignored: false,
  },
  {
    source: '../../../opt/drupal/web/modules/custom/my_module/css/fileB.css',
    errored: false,
    warnings: [],
    deprecations: [],
    invalidOptionWarnings: [],
    ignored: false,
  },
  {
    source: '/builds/my_group/my_project/css/my_module.css',
    errored: false,
    warnings: [],
    deprecations: [],
    invalidOptionWarnings: [],
    ignored: false,
  }
];

const expectedPassingXmlProject = `<?xml version="1.0" encoding="utf-8"?>
<testsuites name="stylelint.rules">
  <testsuite name="file.css" failures="0" errors="0" tests="1">
    <testcase name="stylelint.passed" file="file.css"/>
  </testsuite>
  <testsuite name="my_module/css/file.css" failures="0" errors="0" tests="1">
    <testcase name="stylelint.passed" file="my_module/css/file.css"/>
  </testsuite>
  <testsuite name="web/modules/custom/my_module/css/fileA.css" failures="0" errors="0" tests="1">
    <testcase name="stylelint.passed" file="web/modules/custom/my_module/css/fileA.css"/>
  </testsuite>
  <testsuite name="web/modules/custom/my_module/css/fileB.css" failures="0" errors="0" tests="1">
    <testcase name="stylelint.passed" file="web/modules/custom/my_module/css/fileB.css"/>
  </testsuite>
  <testsuite name="css/my_module.css" failures="0" errors="0" tests="1">
    <testcase name="stylelint.passed" file="css/my_module.css"/>
  </testsuite>
</testsuites>`;

const mockFailingTestProject = [
  {
    source: 'css/fileA.css',
    errored: true,
    warnings: [
      {
        line: 1,
        column: 1,
        rule: 'declaration-block-properties-order',
        severity: 'error',
        text: 'Expected quot;colorquot; to come before quot;font-weightquot; (declaration-block-properties-order)',
      }
    ],
    deprecations: [],
    invalidOptionWarnings: [],
    ignored: false,
  },
  {
    source: '../../../opt/drupal/web/modules/custom/my_module/css/fileB.css',
    errored: true,
    warnings: [
      {
        line: 7,
        column: 3,
        rule: 'declaration-block-properties-order',
        severity: 'error',
        text: 'Expected quot;colorquot; to come before quot;font-weightquot; (declaration-block-properties-order)',
      },
      {
        line: 8,
        column: 3,
        rule: 'shorthand-property-no-redundant-values',
        severity: 'error',
        text: 'Unexpected longhand value #39;0 2rem 1.5rem 2rem#39; instead of #39;0 2rem 1.5rem#39; (shorthand-property-no-redundant-values)',
      },
    ],
    deprecations: [],
    invalidOptionWarnings: [],
    ignored: false,
  },
  {
    source: '/builds/my_group/my_project/css/my_module.css',
    errored: true,
    warnings: [
      {
        line: 8,
        column: 3,
        rule: 'shorthand-property-no-redundant-values',
        severity: 'error',
        text: 'Unexpected longhand value #39;0 2rem 1.5rem 2rem#39; instead of #39;0 2rem 1.5rem#39; (shorthand-property-no-redundant-values)',
      },
    ],
    deprecations: [],
    invalidOptionWarnings: [],
    ignored: false,
  }
];

const expectedFailingXmlProject = `<?xml version="1.0" encoding="utf-8"?>
<testsuites name="stylelint.rules">
  <testsuite name="css/fileA.css" failures="1" errors="1" tests="1">
    <testcase classname="declaration-block-properties-order" name="1:1 > Expected quot;colorquot; to come before quot;font-weightquot; (declaration-block-properties-order)" file="css/fileA.css">
      <failure type="error">On line 1, column 1 in css/fileA.css: Expected quot;colorquot; to come before quot;font-weightquot; (declaration-block-properties-order)</failure>
    </testcase>
  </testsuite>
  <testsuite name="web/modules/custom/my_module/css/fileB.css" failures="2" errors="2" tests="2">
    <testcase classname="declaration-block-properties-order" name="7:3 > Expected quot;colorquot; to come before quot;font-weightquot; (declaration-block-properties-order)" file="web/modules/custom/my_module/css/fileB.css">
      <failure type="error">On line 7, column 3 in web/modules/custom/my_module/css/fileB.css: Expected quot;colorquot; to come before quot;font-weightquot; (declaration-block-properties-order)</failure>
    </testcase>
    <testcase classname="shorthand-property-no-redundant-values" name="8:3 > Unexpected longhand value #39;0 2rem 1.5rem 2rem#39; instead of #39;0 2rem 1.5rem#39; (shorthand-property-no-redundant-values)" file="web/modules/custom/my_module/css/fileB.css">
      <failure type="error">On line 8, column 3 in web/modules/custom/my_module/css/fileB.css: Unexpected longhand value #39;0 2rem 1.5rem 2rem#39; instead of #39;0 2rem 1.5rem#39; (shorthand-property-no-redundant-values)</failure>
    </testcase>
  </testsuite>
  <testsuite name="css/my_module.css" failures="1" errors="1" tests="1">
    <testcase classname="shorthand-property-no-redundant-values" name="8:3 > Unexpected longhand value #39;0 2rem 1.5rem 2rem#39; instead of #39;0 2rem 1.5rem#39; (shorthand-property-no-redundant-values)" file="css/my_module.css">
      <failure type="error">On line 8, column 3 in css/my_module.css: Unexpected longhand value #39;0 2rem 1.5rem 2rem#39; instead of #39;0 2rem 1.5rem#39; (shorthand-property-no-redundant-values)</failure>
    </testcase>
  </testsuite>
</testsuites>`;

const mockEmptyTests = [];

const expectedEmptyXml = `<?xml version="1.0" encoding="utf-8"?>
<testsuites name="stylelint.rules"/>`;

tape('It outputs a correct .xml for passing testsuites', (test) => {
  const output = stylelintFormatter(mockPassingTestProject);
  test.equal(output, expectedPassingXmlProject, 'It matches expectation');
  test.doesNotThrow(() => {
    xml2js.parseString(output, (error) => {
      if (error) throw error;
    });
  }, 'It outputs valid xml');
  test.end();
});

tape('It outputs a correct .xml for failing testsuites', (test) => {
  const output = stylelintFormatter(mockFailingTestProject);
  test.equal(output, expectedFailingXmlProject, 'It matches expectation');
  test.doesNotThrow(() => {
    xml2js.parseString(output, (error) => {
      if (error) throw error;
    });
  }, 'It outputs valid xml');
  test.end();
});

tape('It outputs a correct .xml for empty testsuites', (test) => {
  const output = stylelintFormatter(mockEmptyTests);
  test.equal(output, expectedEmptyXml, 'It matches expectation');
  test.doesNotThrow(() => {
    xml2js.parseString(output, (error) => {
      if (error) throw error;
    });
  }, 'It outputs valid xml');
  test.end();
});
