# Changelog

## [1.0.1] - 2024-04-18

- Update third party, move to ESM
- Fix file path and content for a project

## [1.0.0] - 2022-04-13

- Adapt and rewrite to support JUnit parsing in Gitlab CI
