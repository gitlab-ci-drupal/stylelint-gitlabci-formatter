# Stylelint Gitlab CI formatter

Output JUnit XML reports of Stylelint results for Gitlab CI.

Based on [https://github.com/eddies/stylelint-junit-formatter](https://github.com/eddies/stylelint-junit-formatter), slightly adapted for Gitlab CI.

![Screenshot tests stylelint](https://gitlab.com/gitlab-ci-drupal/stylelint-gitlabci-formatter/uploads/fd6bb74f28a8327847b9a791449ef660/Screenshot_tests_stylelint.jpg)

## Usage

Install stylelint-gitlabci-formatter (and stylelint and optionally, a [config](https://github.com/stylelint/stylelint-config-standard)):

```shell
npm install stylelint-gitlabci-formatter stylelint stylelint-config-standard --save-dev
```

Add a `.stylelintrc`, e.g.:

```javascript
{
  "extends": "stylelint-config-standard",
  "ignoreFiles": "node_modules/**/*",
}
```

Add a script to `package.json` that runs stylelint, e.g.:

```json
{
  "name": "stylelint-ci",
  "version": "1.0.0",
  "scripts": {
    "lint": "stylelint '**/*.css'"
  },
  "devDependencies": {
    "stylelint": "^16",
    "stylelint-gitlabci-formatter": "^1"
  }
}
```

At this point, you should be able to execute `npm run lint` on the command line.

Add a `.gitlab-ci.yml` that runs stylelint and saves the results, e.g.:

```yaml
stylelint:
  stage: lint
  image: node:lts-alpine
  before_script:
    - npm install
  script:
    - npx stylelint
        --custom-formatter ./node_modules/stylelint-gitlabci-formatter
        --output-file stylelint_junit.xml
        "**/*.css"
  artifacts:
    expire_in: 1 week
    when: always
    # https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit
    reports:
      junit: stylelint_junit.xml

```

The formatter will generate a `.xml`-report with the following look:

```xml
<?xml version="1.0" encoding="utf-8"?>
<testsuites name="stylelint.rules">
  <testsuite name="path/to/css/file1.css" failures="0" errors="0" tests="1">
    <testcase name="stylelint.passed"/>
  </testsuite>
  <testsuite name="path/to/css/file2.css" failures="0" errors="0" tests="1">
    <testcase name="stylelint.passed"/>
  </testsuite>
  <testsuite name="path/to/css/file3.css" failures="0" errors="0" tests="1">
    <testcase name="stylelint.passed"/>
  </testsuite>
  <testsuite name="path/to/css/file4.css" failures="0" errors="0" tests="1">
    <testcase name="stylelint.passed"/>
  </testsuite>
</testsuites>
```

In the event of errors, those are presented in a way that Gitlab CI can interpret:

```xml
<?xml version="1.0" encoding="utf-8"?>
<testsuites name="stylelint.rules">
  <testsuite name="path/to/css/file.css" failures="3" errors="3" tests="3">
    <testcase classname="block-closing-brace-space-before" name="1:18 > Expected single space before &quot;}&quot; of a single-line block (block-closing-brace-space-before)" file="path/to/css/file.css">
      <failure type="error">On line 1, column 18 in path/to/css/file.css: Expected single space before "}" of a single-line block (block-closing-brace-space-before)</failure>
    </testcase>
    <testcase classname="declaration-block-trailing-semicolon" name="1:18 > Expected a trailing semicolon (declaration-block-trailing-semicolon)" file="path/to/css/file.css">
      <failure type="error">On line 1, column 18 in path/to/css/file.css: Expected a trailing semicolon (declaration-block-trailing-semicolon)</failure>
    </testcase>
  </testsuite>
</testsuites>
```
