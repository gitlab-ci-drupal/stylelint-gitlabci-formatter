const xmlBuilder = require('xmlbuilder')
const path = require('path')

const {
  CI_WEB_ROOT = '/opt/drupal/web',
  CI_DRUPAL_WEB_ROOT = 'web',
  CI_PROJECT_DIR = '/builds',
} = process.env

// https://stylelint.io/developer-guide/formatters/
function parseFailedCase(testCase, source) {
  const { rule, severity, text, line, column } = testCase

  return {
    '@classname': rule,
    '@name': `${line}:${column} > ${text}`,
    '@file': source.split(path.sep).join('/'),
    failure: {
      '@type': severity,
      '#text': `On line ${line}, column ${column} in ${source}: ${text}`,
    },
  }
}

function parseSuite(testSuite) {
  let filename = testSuite.source
    filename = filename
      .replace(CI_PROJECT_DIR + '/', '')
    filename = filename
      .replace('../../..', '')
      .replace(CI_WEB_ROOT, CI_DRUPAL_WEB_ROOT)

  const suiteName = filename.split(path.sep).join('/')
  const failuresCount = testSuite.warnings.length

  const success = {
    '@name': 'stylelint.passed',
    '@file': `${filename}`,
  }
  const testCases = testSuite.errored ? testSuite.warnings.map((testCase) => parseFailedCase(testCase, suiteName)) : success

  return {
    testsuite: {
      '@name': suiteName,
      '@failures': failuresCount,
      '@errors': failuresCount,
      '@tests': failuresCount || '1',
      testcase: testCases,
    },
  }
}

module.exports = (stylelintResults) => {
  const xmlRoot = xmlBuilder.create('testsuites', { encoding: 'utf-8' }).att('name', 'stylelint.rules')
  const testSuites = stylelintResults.map((testSuite) => parseSuite(testSuite))

  return testSuites.length > 0 ? xmlRoot.element(testSuites).end({ pretty: true }) : xmlRoot.end({ pretty: true })
}
